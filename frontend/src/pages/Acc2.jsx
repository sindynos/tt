import React, { useState, useEffect, useRef } from "react";

export default function Acc2() {
  // const history = useRef([]);
  const count = useRef(0);
  // const interval = useRef(0);

  const DM = ({ children }) => {
    const [motionData, setMotionData] = useState({
      acceleration: {
        x: 0,
        y: 0,
        z: 0,
      },
      accelerationIncludingGravity: {
        x: 0,
        y: 0,
        z: 0,
      },
      interval: 0,
      rotationRate: {
        alpha: 0,
        beta: 0,
        gamma: 0,
      },
    });

    useEffect(() => {
      const handleDeviceMotion = (event) => {
        count.current++;
        const {
          acceleration,
          accelerationIncludingGravity,
          interval,
          rotationRate,
        } = event;

        setMotionData({
          acceleration,
          accelerationIncludingGravity,
          interval,
          rotationRate,
        });
      };

      window.addEventListener("devicemotion", handleDeviceMotion, true);

      return () => {
        window.removeEventListener("devicemotion", handleDeviceMotion, true);
      };
    }, []);

    return children(motionData);
  };

  return (
    <>
      <p>
        Accelerometer (in m/s<sup>2</sup>)
      </p>
      <DM>
        {({
          acceleration,
          accelerationIncludingGravity,
          interval,
          rotationRate,
        }) => (
          <>
            <p>Number of datapoints: {count.current}</p>
            <p>
              Current: <br />
              x: {acceleration.x.toFixed(10)} m/s<sup>2</sup>
              <br />
              y: {acceleration.y.toFixed(10)} m/s<sup>2</sup>
              <br />
              z: {acceleration.z.toFixed(10)} m/s<sup>2</sup>
              <br />
            </p>
            <p>Data interval: {interval} ms</p>
          </>
        )}
      </DM>

      {/*<p>History:</p>*/}
      {/*{history.current.map((d) => (*/}
      {/*  <>*/}
      {/*    <p>*/}
      {/*      x: {d.x}*/}
      {/*      <br />*/}
      {/*      y: {d.y}*/}
      {/*      <br />*/}
      {/*      z: {d.z}*/}
      {/*    </p>*/}
      {/*  </>*/}
      {/*))}*/}
    </>
  );
}
