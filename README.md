# Touch Typing

## Getting started

In the project root, install the dependencies: `npm install`

To start the whole application: `npm start`

To start only the frontend: `npm run frontend`

To start only the backend: `npm run backend`
