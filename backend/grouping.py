import numpy as np
import pandas as pd

def get_grouped_by_key(df, key):
    df = pd.concat(df[key].tolist())
    return df.groupby(df.index)

def array_for_key(df, key):
    df = get_grouped_by_key(df, key)   
    df = df.agg(list)

    return df[0][1:4]

def mean_for_key(df, key, cummulate=False):
    df = get_grouped_by_key(df, key)   
    mean = df.mean()

    if type(mean) == pd.DataFrame:
        mean = mean[0]
    
    mean = mean.tolist()
    
    if cummulate:
        for i in range(1, len(mean)):
            mean[i] += mean[i - 1]

    return mean

def std_range_for_key(df, key, cummulate=False):
    df = get_grouped_by_key(df, key) 
    
    mean = df.mean()
    std = df.std()

    if type(mean) == pd.DataFrame:
        mean = mean[0]
        std = std[0]

    std_min = (mean - std).tolist()
    std_max = (mean + std).tolist()
    mean = mean.tolist()

    if cummulate:
        for i in range(1, len(mean)):
            mean[i] += mean[i - 1]
            std_min[i] += mean[i - 1]
            std_max[i] += mean[i - 1]

    return mean, std_min, std_max

def group_dataframe(df, key, group_key, group_value, op="==", method=mean_for_key):
    subset = df

    if op == "==":
        subset = df[df[group_key] == group_value]
    elif op == "!=":
        subset = df[df[group_key] != group_value]
    elif op == ">=":
        subset = df[df[group_key] >= group_value]
    elif op == "<=":
        subset = df[df[group_key] <= group_value]
    elif op == ">":
        subset = df[df[group_key] > group_value]
    elif op == "<":
        subset = df[df[group_key] < group_value]

    return method(subset, key), len(subset)

def group_into_ranges(df, key, group_key, splitter, label=None, method=mean_for_key):
    results = []
    for min_val, max_val in splitter:
        min_val = min_val or df[group_key].min()
        max_val = max_val or df[group_key].max()
        subset = df[df[group_key] >= min_val]
        subset = subset[subset[group_key] <= max_val]
        label = f"{min_val}-{max_val}"
        results.append(((method(subset, key), len(subset)), label))

    return results

def group_by_unique_values(df, key, group_key, splitter=None, label=None, method=mean_for_key):
    return [(group_dataframe(df, key, group_key, value, "==", method), value) for value in df[group_key].unique()]

def group_into_halfs(df, key, group_key, splitter, labels=None, method=mean_for_key):
    labels = labels or [f"< {splitter}", f">= {splitter}"]

    return [(group_dataframe(df, key, group_key, splitter, "<", method), labels[0]), (group_dataframe(df, key, group_key, splitter, ">=", method), labels[1])]
