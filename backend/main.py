import numpy as np
import pandas as pd
pd.set_option('display.max_columns', None)

from files import *
from preprocessing import *
from grouping import *
from plotting import *

data_folder_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

def load_from_folder():
    df = load_files(data_folder_path)

    total = []
    for index, data in df.iterrows():
        for result in data.results:
            acc = result.pop("acc")
            typing = result.pop("typing")
            errors, phraseLength = process_phrases(typing["providedPhrases"], typing["typedPhrases"])

            result["typing.errors"] = errors
            result["typing.corrections"] = typing["numberOfBackspaces"]
            result["typing.phraseLength"] = phraseLength
            result["typing.speed"] = typing["numberOfCharacters"] / typing["totalTime"] * 1000 # [char/sec]
            result["typing.tlx"] = sum(result.pop("tlx")) * 5 / 6
            result["typing.acc"] = concat_acc(acc, typing["pressTimestamps"])
            result["typing.mean_acc"] = result["typing.acc"]["mean"]

        results = pd.DataFrame(data.pop("results")).set_index("difficulty").sort_index()

        data["formData.age"] = int(data["formData.age"])
        for key in ["typing.errors", "typing.corrections", "typing.phraseLength", "typing.speed", "typing.tlx", "typing.acc", "typing.mean_acc"]:
            tmp = results[key].tolist()
            if key == "typing.acc":
                for i in range(len(tmp) - 1, -1, -1):
                    tmp[i]["mean"] /= tmp[0]["mean"] if i > 0 else tmp[i]["mean"]
            elif key == "typing.errors":
                for i in range(len(tmp) - 1, -1, -1):
                    tmp[i] -= tmp[0] if i > 0 else tmp[i]
            else:
                for i in range(len(tmp) - 1, -1, -1):
                    tmp[i] /= tmp[0] if i > 0 else tmp[i]

            data[key] = pd.DataFrame(tmp)

        total.append(data)
    
    return pd.DataFrame(total)

def create_overall_fig(df, pdf=None):
    fig = create_fig()
    plot_dev(fig.add_subplot(231), [0,1,2,3], std_range_for_key(df, "typing.errors"), "Errors", percent=False)
    plot_dev(fig.add_subplot(232), [0,1,2,3], std_range_for_key(df, "typing.corrections"), "Corrections")
    plot_dev(fig.add_subplot(233), [0,1,2,3], std_range_for_key(df, "typing.speed"), "Speed")
    plot_dev(fig.add_subplot(234), [0,1,2,3], std_range_for_key(df, "typing.tlx"), "TLX", ylim=100)
    plot_dev(fig.add_subplot(235), [0,1,2,3], std_range_for_key(df, "typing.mean_acc"), "Accelerometer")
    if pdf:
        savefig(pdf, False, fig)

def create_overall_box(df, violin=False, pdf=None):
    fig = create_fig()
    plot_box(fig.add_subplot(231), array_for_key(df, "typing.errors"), "Errors", ylim=-5, percent=False, violin=violin)
    plot_box(fig.add_subplot(232), array_for_key(df, "typing.corrections"), "Corrections", violin=violin)
    plot_box(fig.add_subplot(233), array_for_key(df, "typing.speed"), "Speed", violin=violin)
    plot_box(fig.add_subplot(234), array_for_key(df, "typing.tlx"), "TLX", ylim=100, violin=violin)
    plot_box(fig.add_subplot(235), array_for_key(df, "typing.mean_acc"), "Accelerometer", violin=violin)
    if pdf:
        savefig(pdf, False, fig)

def create_attribute_fig(df, attribute, title, ylim=None, percent=True, pdf=None):
    fig = create_fig()
    plot_group(fig.add_subplot(221), [0,1,2,3], group_by_unique_values(df, attribute, "formData.gender"), title + " per Gender", ylim=ylim, percent=percent)
    plot_group(fig.add_subplot(222), [0,1,2,3], group_into_ranges(df, attribute, "formData.age", [(None,24), (25, 29), (30, None)]), title + " per Age Group", ylim=ylim, percent=percent)
    plot_group(fig.add_subplot(223), [0,1,2,3], group_into_halfs(df, attribute, "formData.typing", "rw", ["(rather) poor", "(rather) well"]), title + " per Typing Profiency", ylim=ylim, percent=percent)
    plot_group(fig.add_subplot(224), [0,1,2,3], group_into_halfs(df, attribute, "formData.english", "b2", ["<= B1", ">= B2"]), title + " per English Level", ylim=ylim, percent=percent)
    if pdf:
        savefig(pdf, False, fig)

def create_attribute_dev_fig(df, attribute, title, ylim=None, percent=True, pdf=None):
    fig = create_fig()
    plot_group_dev(fig.add_subplot(221), [0,1,2,3], group_by_unique_values(df, attribute, "formData.gender", method=std_range_for_key), title + " per Gender", ylim=ylim, percent=percent)
    plot_group_dev(fig.add_subplot(222), [0,1,2,3], group_into_ranges(df, attribute, "formData.age", [(None,24), (25, 29), (30, None)], method=std_range_for_key), title + " per Age Group", ylim=ylim, percent=percent)
    plot_group_dev(fig.add_subplot(223), [0,1,2,3], group_into_halfs(df, attribute, "formData.typing", "rw", ["(rather) poor", "(rather) well"], method=std_range_for_key), title + " per Typing Profiency", ylim=ylim, percent=percent)
    plot_group_dev(fig.add_subplot(224), [0,1,2,3], group_into_halfs(df, attribute, "formData.english", "b2", ["<= B1", ">= B2"], method=std_range_for_key), title + " per English Level", ylim=ylim, percent=percent)
    if pdf:
        savefig(pdf, False, fig)

def create_category_fig(df, category, title, method, splitter=None, label=None, pdf=None):
    fig = create_fig()
    plot_group(fig.add_subplot(231), [0,1,2,3], method(df, "typing.errors", category, splitter, label), "Errors per " + title, percent=False)
    plot_group(fig.add_subplot(232), [0,1,2,3], method(df, "typing.corrections", category, splitter, label), "Corrections per " + title)
    plot_group(fig.add_subplot(233), [0,1,2,3], method(df, "typing.speed", category, splitter, label), "Speed per " + title)
    plot_group(fig.add_subplot(234), [0,1,2,3], method(df, "typing.tlx", category, splitter, label), "TLX per " + title, ylim=100)
    plot_group(fig.add_subplot(235), [0,1,2,3], method(df, "typing.mean_acc", category, splitter, label), "Accelerometer per " + title)
    if pdf:
        savefig(pdf, False, fig)

def create_scatter_fig(df, x, y, title, pdf=None):
    x_all = pd.concat(df[x].to_numpy())[0].to_numpy()
    y_all = pd.concat(df[y].to_numpy())[0].to_numpy()

    xlim = (np.min(x_all), np.max(x_all))
    ylim = (np.min(y_all,), np.max(y_all))

    fig = create_fig()
    plot_scatter(fig.add_subplot(131), mean_for_key(df, x), mean_for_key(df, y), title + " - Mean", xlim, ylim)
    plot_scatter(fig.add_subplot(132), x_all, y_all, title + " - All", xlim, ylim)
    plot_multiple_scatter(fig.add_subplot(133), zip(df[x], df[y]), title + " - per Participant", xlim, ylim)
    if pdf:
        savefig(pdf, False, fig)



# *** uncomment the following lines when the preprocessing has changed or new data shall be added ***
# df = load_from_folder()
# df.to_pickle(data_folder_path + "/results_total.pkl")

# load preprocessed data from file
df = pd.read_pickle(data_folder_path + "/results_total.pkl")

pdf = create_pdf(data_folder_path + "/results_analysis.pdf")

# plotting
create_overall_fig(df, pdf=pdf)
create_overall_box(df, pdf=pdf)
create_overall_box(df, violin=True, pdf=pdf)

create_scatter_fig(df, "typing.tlx", "typing.errors", "TLX/Errors", pdf=pdf)
create_scatter_fig(df, "typing.tlx", "typing.corrections", "TLX/Corrections", pdf=pdf)
create_scatter_fig(df, "typing.tlx", "typing.speed", "TLX/Speed", pdf=pdf)
create_scatter_fig(df, "typing.tlx", "typing.mean_acc", "TLX/Accelerometer", pdf=pdf)

create_scatter_fig(df, "typing.speed", "typing.mean_acc", "Speed/Accelerometer", pdf=pdf)

create_attribute_fig(df, "typing.errors", "Errors", percent=False, pdf=pdf)
create_attribute_dev_fig(df, "typing.errors", "Errors", percent=False, pdf=pdf)

create_attribute_fig(df, "typing.corrections", "Corrections", pdf=pdf)
create_attribute_dev_fig(df, "typing.corrections", "Corrections", pdf=pdf)

create_attribute_fig(df, "typing.speed", "Speed", pdf=pdf)
create_attribute_dev_fig(df, "typing.speed", "Speed", pdf=pdf)

create_attribute_fig(df, "typing.tlx", "TLX", ylim=100, pdf=pdf)
create_attribute_dev_fig(df, "typing.tlx", "TLX", ylim=100, pdf=pdf)

create_attribute_fig(df, "typing.mean_acc", "Accelerometer", pdf=pdf)
create_attribute_dev_fig(df, "typing.mean_acc", "Accelerometer", pdf=pdf)

create_category_fig(df, "formData.gender", "Gender", group_by_unique_values, pdf=pdf)
create_category_fig(df, "formData.age", "Age Group", group_into_ranges, splitter=[(None,24), (25, 29), (30, None)], pdf=pdf)
create_category_fig(df, "formData.typing", "Typing Profiency", group_into_halfs, splitter="rw", label=["(rather) poor", "(rather) well"], pdf=pdf)
create_category_fig(df, "formData.english", "English Profiency", group_into_halfs, splitter="b2", label=["<= B1", ">= B2"])

savefig(pdf, True)
#show()