import numpy as np
import pandas as pd
import Levenshtein


def calc_acc_length(entry):
    x = entry["acceleration"]["x"]
    y = entry["acceleration"]["y"]
    z = entry["acceleration"]["z"]
    return (x ** 2 + y ** 2 + z ** 2) ** 0.5

def filter_acc_values(acc, timestamps):
    result = []
    for value in acc:
        for i in range(value["interval"]):
            if value["timestamp"] + i in timestamps:
                result.append(value)
                break
    
    return result

def concat_acc(acc, timestamps):
    min_t = timestamps[0]
    max_t = timestamps[len(timestamps) - 1]

    acc = filter(lambda value: value["timestamp"] >= min_t and value["timestamp"] <= max_t, acc)    # instead mapping to timestamps within interval? (with offset between key event and measurement)
    acc = map(calc_acc_length, acc)
    acc = filter(lambda value: value > 0, acc)  # ignore not-moving entries
    acc = list(acc)

    return {
        "min": np.min(acc),
        "max": np.max(acc),
        "mean": np.mean(acc),
        "std": np.std(acc)
    }

def process_phrases(provided, typed):
    typed = list(filter(lambda phrase: len(phrase) > 0, typed))
    result = 0
    length = 0
    for i in range(len(provided)):
        length += len(provided[i])
        result += Levenshtein.distance(provided[i], typed[i])

    length /= len(provided)

    return result, length
