from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np

create_table = False

def plot_simple(ax, x, y, title, ylim=None, percent=True):
    if percent:
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())

    ax.plot(x, y if not percent else [i * 100 for i in y])
    ax.legend()
    ax.set_title(title)
    plt.xticks(x)
    plt.ylim(bottom=ylim or 0)
    plt.margins(0)

    if create_table:
        create_table_simple(x, y, title)

def create_table_simple(x, y, title):
    from rich.console import Console
    from rich.table import Table

    table = Table(title=title)

    table.add_column("n-back Level", style="cyan", no_wrap=True)
    table.add_column("Mean", style="magenta")

    for index, value in enumerate(x):
        table.add_row(str(value), str(y[index]))

    console = Console()
    console.print(table)

def plot_dev(ax, x, data, title, ylim=None, percent=True):
    mean, std_min, std_max = data

    if percent:
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
        mean = [i * 100 for i in mean]
        std_min = [i * 100 for i in std_min]
        std_max = [i * 100 for i in std_max]

    ax.plot(x, mean, "k-")
    ax.fill_between(x, std_min, std_max, alpha=0.5)
    ax.set_title(title)
    
    plt.xticks(x)
    plt.ylim(bottom=ylim or 0)
    plt.margins(0)
 
    if create_table:
        create_table_dev(x, data, title)

def create_table_dev(x, data, title):
    from rich.console import Console
    from rich.table import Table

    table = Table(title=title)

    table.add_column("n-back Level", style="cyan", no_wrap=True)
    table.add_column("Mean", style="magenta")
    table.add_column("Std. Min", style="green")
    table.add_column("Std. Max", style="red")

    mean, std_min, std_max = data

    for index, value in enumerate(x):
        if type(mean) is not list:
            mean = mean.values.tolist()
        table.add_row(str(value), str(mean[index]), str(std_min[index]), str(std_max[index]))

    console = Console()
    console.print(table)

def plot_group(ax, x, group, title, ylim=None, percent=True):
    if percent:
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())

    for data, label in group:
        data, length = data
        ax.plot(x, data if not percent else [i * 100 for i in data], label=f"{label} (n={length})")

    ax.legend()
    ax.set_title(title)
    plt.xticks(x)
    plt.ylim(bottom=ylim or 0)
    plt.margins(0)

    if create_table:
        create_table_group(x, group, title)

def create_table_group(x, group, title):
    from rich.console import Console
    from rich.table import Table

    table = Table(title=title)

    table.add_column("n-back Level", style="cyan", no_wrap=True)
    table.add_column("Group", style="yellow")
    table.add_column("Mean", style="magenta")

    for index, value in enumerate(x):
        for data, label in group:
            data, length = data
            if type(data) is not list:
                data = data.values.tolist()
            table.add_row(str(value), f"{label} (n={length})", str(data[index]))
        table.add_section()

    console = Console()
    console.print(table)

def plot_group_dev(ax, x, group, title, ylim=None, percent=True):    
    if percent:
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())

    for data, label in group:
        data, length = data
        mean, std_min, std_max = data

        if percent:
            mean = [i * 100 for i in mean]
            std_min = [i * 100 for i in std_min]
            std_max = [i * 100 for i in std_max]

        ax.plot(x, mean, "-", label=f"{label} (n={length})")
        ax.fill_between(x, std_min, std_max, alpha=0.25)

    ax.legend()
    ax.set_title(title)
    plt.xticks(x)
    plt.ylim(bottom=ylim or 0)
    plt.margins(0)

    if create_table:
        create_table_group_dev(x, group, title)

def create_table_group_dev(x, group, title):
    from rich.console import Console
    from rich.table import Table
    
    table = Table(title=title)

    table.add_column("n-back level", style="cyan", no_wrap=True)
    table.add_column("Group", style="yellow")
    table.add_column("Mean", style="magenta")
    table.add_column("Std. Min", style="green")
    table.add_column("Std. Max", style="red")

    for index, value in enumerate(x):
        for data, label in group:
            data, length = data
            mean, std_min, std_max = data
            if type(mean) is not list:
                mean = mean.values.tolist()
            table.add_row(str(value), f"{label} (n={length})", str(mean[index]), str(std_min[index]), str(std_max[index]))
        table.add_section()

    console = Console()
    console.print(table)

def plot_box(ax, data, title, ylim=None, percent=True, violin=False):
    if percent:
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
        data_copy = []
        for entry in data:
            data_copy.append([x * 100 for x in entry])

        data = np.array(data_copy).T

    if violin:
        ax.violinplot(data, showmeans=False, showmedians=True)
    else:
        ax.boxplot(data, showfliers=False, boxprops=dict(linestyle="-", linewidth=2))

    ax.set_title(title)

    plt.ylim(bottom=ylim or 0)
    plt.margins(0)

def plot_scatter(ax, x, y, title, xlim, ylim):
    b,a = np.polyfit(x, y, deg=1)
    xseq = np.linspace(xlim[0], xlim[1], num=2)

    ax.scatter(x, y)
    ax.plot(xseq, a + b * xseq,  alpha=0.5, linewidth=0.5)

    ax.set_title(title)
    plt.margins(0)
    plt.xlim(xlim)
    plt.ylim(ylim)

def plot_multiple_scatter(ax, data, title, xlim, ylim):
    for x, y in data:
        x = x[0].to_numpy()
        y = y[0].to_numpy()
        
        b,a = np.polyfit(x, y, deg=1)
        xseq = np.linspace(xlim[0], xlim[1], num=2)
        ax.scatter(x, y)
        ax.plot(xseq, a + b * xseq, alpha=0.5, linewidth=0.5)
    
    ax.set_title(title)
    plt.margins(0)
    plt.xlim(xlim)
    plt.ylim(ylim)

def create_pdf(name):
    return PdfPages(name)

def create_fig():
    return plt.figure(figsize=(11.69, 8.27))

def show():
    plt.tight_layout()
    plt.show()

def savefig(pdf, close=True, fig=None):
    plt.tight_layout()
    pdf.savefig()

    if close:
        pdf.close()
    if fig:
        plt.close(fig)
